//
//  ViewController.swift
//  CurrencyExchangeRate
//
//  Created by user196354 on 5/6/21.
//

import UIKit

// Buggy:   Currencies will flash if it takes a second for it to load from api
//          Could just let the placeholder stay
class CurrencyViewController: UIViewController {

    var currencies = [CurrencyModel]()
    let currencyPickerView = UIPickerView.init()
    var manager = CurrencyExchangeManager()
    var selectedRow = 0
    var selectedRow2 = 0
    
    @IBOutlet weak var currencyTextField2: UITextField!
    @IBOutlet weak var currencyTextField1: UITextField!
    @IBOutlet weak var amountTextField: UITextField!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var convertButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        currencyPickerView.dataSource = self
        currencyPickerView.delegate = self
        currencyTextField1.delegate = self
        currencyTextField2.delegate = self
        amountTextField.delegate = self
        manager.delegate = self

        initTextField(for: currencyTextField1)
        initTextField(for: currencyTextField2)
        
        manager.fetchConversionRates()
        
        convertButton.layer.cornerRadius = 10
        
    }
    
    func convertAndSetLabel() {
        let currency1 = self.currencies[self.currencies.firstIndex(where: {$0.currencyName == self.currencyTextField1.text})!]
        let currency2 = self.currencies[self.currencies.firstIndex(where: {$0.currencyName == self.currencyTextField2.text})!]
        
        
        // If the textfield is empty, we assume it's zero as the default value
        let textFieldEmptyToZero = self.amountTextField.text! == "" ? "0" : self.amountTextField.text!
        
        // Because floats only accepts dot notation, but writing currencies can be with a comma, we replace commas with dot/point
        let textFieldToDot = textFieldEmptyToZero.replacingOccurrences(of: ",", with: ".")
        if let floatValue = Float(textFieldToDot) {
            self.amountLabel.text = String(currency1.convertRate(to: currency2, amount: floatValue))
        } else {
            self.amountLabel.text = "Not a valid currency format"
        }
    }
    
    /// Initiate the given `textField`
    func initTextField(for textField: UITextField) {
        // Set initial text to first item in the `currencies` array
        textField.text = "currencies"
        
        // Replace our keyboardview with a custom PickerView
        textField.inputView = currencyPickerView
        textField.tintColor = UIColor.clear
        
        // Create a toolbar so users can press done when they select a row
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.sizeToFit()
        
        // Create the button for the toolbar
        let doneButton = UIBarButtonItem()
        doneButton.style = UIBarButtonItem.Style.plain
        doneButton.primaryAction = UIAction(title: "Done") { action in
            textField.text = self.currencies[self.currencyPickerView.selectedRow(inComponent: 0)].currencyName
            
            // Reset the picker
            self.currencyPickerView.selectRow(0, inComponent: 0, animated: false)
            
            // If you want the label to get set after an user picked something from the
            // UIPicker, uncomment this.
            // self.convertAndSetLabel()
            
            super.view.endEditing(true)
        }
        
        toolBar.setItems([doneButton], animated: false)
        toolBar.tintColor = .black
        textField.inputAccessoryView = toolBar
    }
    
    @IBAction func convertButtonPressed(_ sender: UIButton) {
        convertAndSetLabel()
    }
}

extension CurrencyViewController: UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return currencies.count
    }
}

extension CurrencyViewController: UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return currencies[row].currencyName
    }
}

extension CurrencyViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == amountTextField {
            return true
        }
        return false
    }
        
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {

//        We are using the button to set the label to the correct amount
        //If you want the label to be set after the currency or amount is changed, uncomment this.
//        if (textField == amountTextField) {
//            convertAndSetLabel()
//        }
    }
}

extension CurrencyViewController: CurrencyExchangeManagerDelegate {
    func didFetchRates(_ currencyExchangeManager: CurrencyExchangeManager, currencyRates: [CurrencyModel]) {
        let sortedRates = currencyRates.sorted(by: {$0.currencyName < $1.currencyName})
        currencies = sortedRates
        
        DispatchQueue.main.async {
            self.currencyTextField1.text = self.currencies[self.currencyPickerView.selectedRow(inComponent: 0)].currencyName
            self.currencyTextField2.text = self.currencies[self.currencyPickerView.selectedRow(inComponent: 0)].currencyName
        }
    }
    
    func didFailWithError(_ error: Error) {
        print("didFailWithError")
    }
    
    
}

