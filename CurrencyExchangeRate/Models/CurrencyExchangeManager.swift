//
//  CurrencyExchangeManager.swift
//  CurrencyExchangeRate
//
//  Created by user196354 on 5/7/21.
//

import Foundation

protocol CurrencyExchangeManagerDelegate {
    func didFetchRates(_ currencyExchangeManager :CurrencyExchangeManager, currencyRates: [CurrencyModel])
    func didFailWithError(_ error: Error)
}

struct CurrencyExchangeManager {
    let baseAPIEndpoint = "https://v6.exchangerate-api.com/v6/"
    var delegate: CurrencyExchangeManagerDelegate?
    
    /// Fetch all the currency exchange rates at once
    /// This has some implications, namely, exchange rates changes rapidly.
    /// But for learning purposes, I will just fetch everything at once
    func fetchConversionRates() {
        let url = "\(baseAPIEndpoint)\(Keys.exchangeRateApi)/latest/EUR"
        performRequest(with: url)
    }
    
    func performRequest(with url: String) {
        
        let urlString = URL(string: url)
        if let safeUrl = urlString {
            let session = URLSession(configuration: .default)
            let task = session.dataTask(with: safeUrl) { (data, response, error) in
                if error != nil {
                    print("error")
                }
                if let safeData = data {
                    // Using the delegate approach to return data.
                    // Not sure whether I like this approach, since I'm used to returning data / throwing data directly
                    if let parsedData = self.parseJSON(safeData) {
                        self.delegate?.didFetchRates(self, currencyRates: parsedData)
                    }
                }
            }
            task.resume()
        }
    }

    func parseJSON(_ data: Data) -> [CurrencyModel]? {
        
        let jsonDecoder = JSONDecoder()
        do {
            let decodedData = try jsonDecoder.decode(CurrencyData.self, from: data)
            let currencyRates = decodedData.conversion_rates
            
            var resultCurrencies = [CurrencyModel]()
            for currencyRate in currencyRates {
                let name = currencyRate.key
                let rate = currencyRate.value
                
                resultCurrencies.append(CurrencyModel(currencyName: name, rate: rate))
            }
            
            return resultCurrencies
            
        } catch {
            print("Error happened")
            return nil
        }
    }
}
