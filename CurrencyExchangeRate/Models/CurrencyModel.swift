//
//  CurrencyModel.swift
//  CurrencyExchangeRate
//
//  Created by user196354 on 5/7/21.
//

import Foundation

struct CurrencyModel {
    let currencyName: String
    let rate: Float
    
    /// Converts the `amount` to `currency`
    func convertRate(to currency: CurrencyModel, amount: Float) -> Float {
        if rate == currency.rate {
            return amount
        }
        
        // return 0 if 0 because we can't divide by 0
        if amount == 0 {
            return 0
        }
        
        let convertedAmount = amount / self.rate * currency.rate
        return convertedAmount
    }
}
