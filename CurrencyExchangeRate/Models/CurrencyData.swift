//
//  CurrencyData.swift
//  CurrencyExchangeRate
//
//  Created by user196354 on 5/7/21.
//

import Foundation

struct CurrencyData: Codable {
    let time_last_update_unix: Int
    let conversion_rates: Dictionary<String, Float>
}
