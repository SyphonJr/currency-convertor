# Currency convertor

First app project made with Swift. User is able to convert between different currencies.

Design taken from [here](https://miro.medium.com/max/10704/1*VyerXSttKlWek9Kub8HoTA.png)

## Image of project

![](currency_convertor_img.png)

## What is in this project

- MVC-architecture
- Fetching from api
- Storyboard
